use crate::object::Object;

#[derive(Debug)]
pub struct Type(*const ffi::Babl);

impl Object for Type {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Type {
    pub fn from_type(data_type: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_type(data_type.as_ptr() as *const i8)) }
    }
}
