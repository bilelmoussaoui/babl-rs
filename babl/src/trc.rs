use crate::object::Object;

#[derive(Debug)]
pub struct Trc(*const ffi::Babl);

impl Object for Trc {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Trc {
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_trc(name.as_ptr() as *const i8)) }
    }

    pub fn from_gamma(gamma: f64) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_trc_gamma(gamma)) }
    }
}
