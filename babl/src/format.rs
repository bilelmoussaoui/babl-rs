use crate::model::Model;
use crate::object::Object;
use crate::space::Space;
use crate::types::Type;
use std::ffi::CStr;

#[derive(Debug)]
pub struct Format(*const ffi::Babl);

impl Object for Format {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Format {
    /// Returns the babl object representing the color format given by
    ///
    /// @name such as for example \"RGB u8\", \"CMYK float\" or \"CIE Lab u16\",
    /// creates a format using the sRGB space, to also specify the color space
    /// and TRCs for a format, see babl_format_with_space.
    pub fn from_encoding(encoding: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_format(encoding.as_ptr() as *const i8)) }
    }

    pub fn format_with_space(encoding: &str, space: &Space) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_format_with_space(
                encoding.as_ptr() as *const i8,
                space.inner(),
            ))
        }
    }

    pub fn exists(format_name: &str) -> bool {
        unsafe { ffi::babl_format_exists(format_name.as_ptr() as *const i8) == 1 }
    }

    pub fn get_bytes_per_pixel(&self) -> i32 {
        unsafe { ffi::babl_format_get_bytes_per_pixel(self.0) as i32 }
    }

    pub fn get_encoding(&self) -> String {
        unsafe {
            CStr::from_ptr(ffi::babl_format_get_encoding(self.0))
                .to_string_lossy()
                .into()
        }
    }

    pub fn get_model(&self) -> Model {
        unsafe { Model::from_raw_full(ffi::babl_format_get_model(self.0)) }
    }

    pub fn get_space(&self) -> Space {
        unsafe { Space::from_raw_full(ffi::babl_format_get_space(self.0)) }
    }

    pub fn get_type(&self, component_index: i32) -> Type {
        unsafe { Type::from_raw_full(ffi::babl_format_get_type(self.0, component_index)) }
    }

    pub fn get_n_components(&self) -> i32 {
        unsafe { ffi::babl_format_get_n_components(self.0) as i32 }
    }

    pub fn has_alpha(&self) -> bool {
        unsafe { ffi::babl_format_has_alpha(self.0) == 1 }
    }

    pub fn is_format_n(&self) -> bool {
        unsafe { ffi::babl_format_is_format_n(self.0) == 1 }
    }

    pub fn is_palette(&self) -> bool {
        unsafe { ffi::babl_format_is_palette(self.0) == 1 }
    }

    pub fn format_n(type_: &Type, components: i32) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_format_n(type_.inner(), components)) }
    }
}

#[cfg(test)]
mod tests {
    use super::Format;
    use crate::functions::{exit, init};
    use crate::object::Object;

    #[test]
    fn basic_format() {
        init();

        let color_format = Format::from_encoding("CMYK float");

        assert_eq!(color_format.get_bytes_per_pixel(), 16);
        assert_eq!(color_format.get_n_components(), 4);
        assert_eq!(color_format.is_palette(), false);
        assert_eq!(color_format.is_format_n(), false);
        assert_eq!(color_format.has_alpha(), false);
        assert_eq!(color_format.get_type(1).get_name(), "float");

        assert_eq!(color_format.get_name(), "CMYK float");
        assert_eq!(color_format.get_encoding(), "CMYK float");

        exit();
    }

    #[test]
    fn test_format_exists() {
        init();

        assert_eq!(Format::exists("CMYK float"), true);
        assert_eq!(Format::exists("RGB u8"), false);
        assert_eq!(Format::exists("CIE Lab u16"), false);

        assert_eq!(Format::exists("CMYK d"), false);
        exit();
    }
}
