extern crate babl_sys as ffi;
extern crate libc;

pub const ALPHA_FLOOR: f64 = ffi::BABL_ALPHA_FLOOR;
pub const ALPHA_FLOOR_F: f64 = ffi::BABL_ALPHA_FLOOR_F;

mod component;
mod enums;
mod format;
mod functions;
mod model;
mod object;
mod sampling;
mod space;
mod trc;
mod types;

pub use component::Component;
pub use format::Format;
pub use functions::{exit, get_version, init};
pub use model::Model;
pub use object::Object;
pub use sampling::Sampling;
pub use space::Space;
pub use trc::Trc;
pub use types::Type;
