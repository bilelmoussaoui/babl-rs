use crate::object::Object;

#[derive(Debug)]
pub struct Sampling(*const ffi::Babl);

impl Object for Sampling {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Sampling {
    pub fn new(horizontal: i32, vertical: i32) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_sampling(horizontal, vertical)) }
    }
}
