use enumflags2::BitFlags;

pub enum Error {
    Space(String),
}

#[derive(Debug, Clone, Copy, BitFlags)]
#[repr(u32)]
pub enum ModelFlag {
    /// the model encodes alpha.
    Alpha = 2,
    /// the alpha is associated alpha.
    Associated = 4,
    /// the components are inverted (used for getting the additive complement space of CMYK).
    Inverted = 8,
    /// the data has no TRC, i.e. is linear
    Linear = 1024,
    /// the data has a TRC - the TRC from the configured space
    NonLinear = 2048,
    /// the data has a TRC - a perceptual TRC where 50% gray is 0.5
    Perceptual = 4096,
    /// this is a gray component model
    Grey = 1048576,
    /// this is an RGB based component model, the space associated is expected to contain an RGB matrix profile.
    Rgb = 2097152,
    /// this model is part of the CIE family of spaces
    Cie = 8388608,
    /// the encodings described are CMYK encodings, the space associated is expected to contain an CMYK ICC profile.
    Cmyk = 16777216,
}

#[derive(Debug, Clone, Copy)]
pub enum SpaceFlags {
    None,
    Equalize,
    __Unknown(u32),
}

impl From<ffi::BablSpaceFlags> for SpaceFlags {
    fn from(space_flags: ffi::BablSpaceFlags) -> Self {
        match space_flags {
            ffi::BablSpaceFlags_BABL_SPACE_FLAG_NONE => Self::None,
            ffi::BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE => Self::Equalize,
            u => Self::__Unknown(u),
        }
    }
}

impl Into<ffi::BablSpaceFlags> for SpaceFlags {
    fn into(self) -> ffi::BablSpaceFlags {
        match self {
            Self::None => ffi::BablSpaceFlags_BABL_SPACE_FLAG_NONE,
            Self::Equalize => ffi::BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE,
            Self::__Unknown(u) => u,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum IccIntent {
    Perceptual,
    RelativeColorimetric,
    Saturation,
    AbsoluteColorimetric,
    Performance,
    __Unknown(u32),
}

impl From<ffi::BablIccIntent> for IccIntent {
    fn from(icc_intent: ffi::BablIccIntent) -> Self {
        match icc_intent {
            ffi::BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL => Self::Perceptual,
            ffi::BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC => Self::RelativeColorimetric,
            ffi::BablIccIntent_BABL_ICC_INTENT_SATURATION => Self::Saturation,
            ffi::BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC => Self::AbsoluteColorimetric,
            ffi::BablIccIntent_BABL_ICC_INTENT_PERFORMANCE => Self::Performance,
            u => Self::__Unknown(u),
        }
    }
}

impl Into<ffi::BablIccIntent> for IccIntent {
    fn into(self) -> ffi::BablIccIntent {
        match self {
            Self::Perceptual => ffi::BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL,
            Self::RelativeColorimetric => ffi::BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC,
            Self::Saturation => ffi::BablIccIntent_BABL_ICC_INTENT_SATURATION,
            Self::AbsoluteColorimetric => ffi::BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC,
            Self::Performance => ffi::BablIccIntent_BABL_ICC_INTENT_PERFORMANCE,
            Self::__Unknown(u) => u,
        }
    }
}
