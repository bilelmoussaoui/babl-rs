pub fn get_version() -> (i32, i32, i32) {
    unsafe {
        let mut major: i32 = 0;
        let mut minor: i32 = 0;
        let mut micro: i32 = 0;

        ffi::babl_get_version(&mut major, &mut minor, &mut micro);
        (major as i32, minor as i32, micro as i32)
    }
}

pub fn init() {
    unsafe {
        ffi::babl_init();
    }
}

pub fn exit() {
    unsafe {
        ffi::babl_exit();
    }
}

#[cfg(test)]
mod tests {
    use super::get_version;

    #[test]
    fn version() {
        assert_eq!(get_version(), (0, 1, 78));
    }
}
