use crate::{
    enums::{Error, IccIntent, SpaceFlags},
    object::Object,
    trc::Trc,
};
use std::convert::TryInto;

#[derive(Debug)]
pub struct Space(*const ffi::Babl);

impl Object for Space {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Space {
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_space(name.as_ptr() as *const i8)) }
    }

    pub fn from_icc(icc_data: Vec<u8>, intent: IccIntent) -> Result<Self, Error> {
        unsafe {
            let error = "";

            let space = ffi::babl_space_from_icc(
                icc_data.as_ptr() as *const i8,
                icc_data.len().try_into().unwrap(),
                intent.into(),
                error.as_ptr() as *mut *const i8,
            );

            match error {
                "" => Ok(Self::from_raw_full(space)),
                err => Err(Error::Space(err.to_string())),
            }
        }
    }

    pub fn from_chromaticities(
        name: &str,
        wx: f64,
        wy: f64,
        rx: f64,
        ry: f64,
        gx: f64,
        gy: f64,
        bx: f64,
        by: f64,
        trc_red: &Trc,
        trc_green: &Trc,
        trc_blue: &Trc,
        flags: SpaceFlags,
    ) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_space_from_chromaticities(
                name.as_ptr() as *const i8,
                wx,
                wy,
                rx,
                ry,
                gx,
                gy,
                bx,
                by,
                trc_red.inner(),
                trc_green.inner(),
                trc_blue.inner(),
                flags.into(),
            ))
        }
    }

    pub fn from_rgbxyz_matrix(
        name: &str,
        wx: f64,
        wy: f64,
        wz: f64,
        rx: f64,
        gx: f64,
        bx: f64,
        ry: f64,
        gy: f64,
        by: f64,
        rz: f64,
        gz: f64,
        bz: f64,
        trc_red: &Trc,
        trc_green: &Trc,
        trc_blue: &Trc,
    ) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_space_from_rgbxyz_matrix(
                name.as_ptr() as *const i8,
                wx,
                wy,
                wz,
                rx,
                gx,
                bx,
                ry,
                gy,
                by,
                rz,
                gz,
                bz,
                trc_red.inner(),
                trc_green.inner(),
                trc_blue.inner(),
            ))
        }
    }

    pub fn with_trc(self, trc: &Trc) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_space_with_trc(self.0, trc.inner())) }
    }

    pub fn get_gamma(&self) -> f64 {
        unsafe { ffi::babl_space_get_gamma(self.0) }
    }

    pub fn get_rgb_luminance(&self) -> (f64, f64, f64) {
        unsafe {
            let mut luminance = (0.0, 0.0, 0.0);
            ffi::babl_space_get_rgb_luminance(
                self.0,
                &mut luminance.0,
                &mut luminance.1,
                &mut luminance.2,
            );
            (luminance.0 as f64, luminance.1 as f64, luminance.2 as f64)
        }
    }

    pub fn is_cmyk(&self) -> bool {
        unsafe { ffi::babl_space_is_cmyk(self.0) == 1 }
    }

    pub fn is_gray(&self) -> bool {
        unsafe { ffi::babl_space_is_gray(self.0) == 1 }
    }

    pub fn get(
        &self,
        xw: &mut f64,
        yw: &mut f64,
        xr: &mut f64,
        yr: &mut f64,
        xg: &mut f64,
        yg: &mut f64,
        xb: &mut f64,
        yb: &mut f64,
        red_trc: &mut Trc,
        green_trc: &mut Trc,
        blue_trc: &mut Trc,
    ) {
        unsafe {
            ffi::babl_space_get(
                self.0,
                xw,
                yw,
                xr,
                yr,
                xg,
                yg,
                xb,
                yb,
                &mut red_trc.inner(),
                &mut green_trc.inner(),
                &mut blue_trc.inner(),
            )
        }
    }
}
