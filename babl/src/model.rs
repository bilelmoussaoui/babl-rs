use crate::enums::ModelFlag;
use crate::object::Object;
use crate::space::Space;
use enumflags2::BitFlags;

#[derive(Debug)]
pub struct Model(*const ffi::Babl);

impl Object for Model {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Model {
    pub fn from_name_with_space(name: &str, space: &Space) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_model_with_space(
                name.as_ptr() as *const i8,
                space.inner(),
            ))
        }
    }

    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_model(name.as_ptr() as *const i8)) }
    }

    pub fn get_flags(&self) -> BitFlags<ModelFlag> {
        unsafe { BitFlags::new(ffi::babl_get_model_flags(self.0)) }
    }

    /// Returns whether it's the model name or not
    pub fn model_is(&self, name: &str) -> bool {
        unsafe { ffi::babl_model_is(self.0, name.as_ptr() as *const i8) == 1 }
    }
}
