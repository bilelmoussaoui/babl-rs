use crate::object::Object;

#[derive(Debug)]
pub struct Component(*const ffi::Babl);

impl Object for Component {
    unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
        assert!(!ptr.is_null());
        Self(ptr)
    }

    unsafe fn inner(&self) -> *const ffi::Babl {
        self.0
    }
}

impl Component {
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_component(name.as_ptr() as *const i8)) }
    }
}
