#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

pub const BABL_MAJOR_VERSION: u32 = 0;
pub const BABL_MINOR_VERSION: u32 = 1;
pub const BABL_MICRO_VERSION: u32 = 73;
pub const BABL_ALPHA_FLOOR: f64 = 0.0000152587890625;
pub const BABL_ALPHA_FLOOR_F: f64 = 0.0000152587890625;
#[repr(C)]
#[derive(Copy, Clone)]
pub struct _Babl {
    _unused: [u8; 0],
}
/// Babl:
///
/// The babl API is based around polymorphism and almost everything is
/// a Babl object.
pub type Babl = _Babl;
pub type BablFuncLinear = ::std::option::Option<
    unsafe extern "C" fn(
        conversion: *const Babl,
        src: *const ::std::os::raw::c_char,
        dst: *mut ::std::os::raw::c_char,
        n: ::std::os::raw::c_long,
        user_data: *mut ::std::os::raw::c_void,
    ),
>;
pub type BablFuncPlanar = ::std::option::Option<
    unsafe extern "C" fn(
        conversion: *const Babl,
        src_bands: ::std::os::raw::c_int,
        src: *mut *const ::std::os::raw::c_char,
        src_pitch: *mut ::std::os::raw::c_int,
        dst_bands: ::std::os::raw::c_int,
        dst: *mut *mut ::std::os::raw::c_char,
        dst_pitch: *mut ::std::os::raw::c_int,
        n: ::std::os::raw::c_long,
        user_data: *mut ::std::os::raw::c_void,
    ),
>;
extern "C" {
    /// babl_get_version:
    /// @major: (out): The major version number
    /// @minor: (out): The minor version number
    /// @micro: (out): The micro version number
    ///
    /// Get the version information on the babl library
    pub fn babl_get_version(
        major: *mut ::std::os::raw::c_int,
        minor: *mut ::std::os::raw::c_int,
        micro: *mut ::std::os::raw::c_int,
    );
}
extern "C" {
    /// babl_init:
    ///
    /// Initializes the babl library.
    pub fn babl_init();
}
extern "C" {
    /// babl_exit:
    ///
    /// Deinitializes the babl library and frees any resources used when
    /// matched with the number of calls to babl_init().
    pub fn babl_exit();
}
extern "C" {
    /// babl_type:
    ///
    /// Returns the babl object representing the data type given by @name
    /// such as for example \"u8\", \"u16\" or \"float\".
    pub fn babl_type(name: *const ::std::os::raw::c_char) -> *const Babl;
}
extern "C" {
    /// babl_sampling:
    ///
    /// Returns the babl object representing the @horizontal and @vertical
    /// sampling such as for example 2, 2 for the chroma components in
    /// YCbCr.
    pub fn babl_sampling(
        horizontal: ::std::os::raw::c_int,
        vertical: ::std::os::raw::c_int,
    ) -> *const Babl;
}
extern "C" {
    /// babl_component:
    ///
    /// Returns the babl object representing the color component given by
    /// @name such as for example \"R\", \"cyan\" or \"CIE L\".
    pub fn babl_component(name: *const ::std::os::raw::c_char) -> *const Babl;
}
extern "C" {
    /// babl_model:
    ///
    /// Returns the babl object representing the color model given by @name
    /// such as for example \"RGB\", \"CMYK\" or \"CIE Lab\".
    pub fn babl_model(name: *const ::std::os::raw::c_char) -> *const Babl;
}
extern "C" {
    /// babl_model_with_space:
    ///
    /// The models for formats also have a space in babl, try to avoid code
    /// needing to use this.
    pub fn babl_model_with_space(
        name: *const ::std::os::raw::c_char,
        space: *const Babl,
    ) -> *const Babl;
}
extern "C" {
    /// babl_space:
    ///
    /// Returns the babl object representing the specific RGB matrix color
    /// working space referred to by name. Babl knows of:
    ///    sRGB, Rec2020, Adobish, Apple and ProPhoto
    ///
    pub fn babl_space(name: *const ::std::os::raw::c_char) -> *const Babl;
}
pub const BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL: BablIccIntent = 0;
pub const BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC: BablIccIntent = 1;
pub const BablIccIntent_BABL_ICC_INTENT_SATURATION: BablIccIntent = 2;
pub const BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC: BablIccIntent = 3;
pub const BablIccIntent_BABL_ICC_INTENT_PERFORMANCE: BablIccIntent = 32;
pub type BablIccIntent = ::std::os::raw::c_uint;
extern "C" {
    /// babl_space_from_icc:
    /// @icc_data: pointer to icc profile in memory
    /// @icc_length: length of icc profile in bytes
    /// @intent: the intent from the ICC profile to use.
    /// @error: (out): pointer to a string where decoding errors can be stored,
    ///         if an error occurs, NULL is returned and an error message
    ///         is provided in error.
    ///
    /// Create a babl space from an in memory ICC profile, the profile does no
    /// longer need to be loaded for the space to work, multiple calls with the same
    /// icc profile and same intent will result in the same babl space.
    ///
    /// On a profile that doesn't contain A2B0 and B2A0 CLUTs perceptual and
    /// relative-colorimetric intents are treated the same.
    ///
    /// If a BablSpace cannot be created from the profile NULL is returned and a
    /// static string is set on the const char *value pointed at with &value
    /// containing a message describing why the provided data does not yield a babl
    /// space.
    pub fn babl_space_from_icc(
        icc_data: *const ::std::os::raw::c_char,
        icc_length: ::std::os::raw::c_int,
        intent: BablIccIntent,
        error: *mut *const ::std::os::raw::c_char,
    ) -> *const Babl;
}
extern "C" {
    pub fn babl_icc_make_space(
        icc_data: *const ::std::os::raw::c_char,
        icc_length: ::std::os::raw::c_int,
        intent: BablIccIntent,
        error: *mut *const ::std::os::raw::c_char,
    ) -> *const Babl;
}
extern "C" {
    pub fn babl_icc_get_key(
        icc_data: *const ::std::os::raw::c_char,
        icc_length: ::std::os::raw::c_int,
        key: *const ::std::os::raw::c_char,
        language: *const ::std::os::raw::c_char,
        country: *const ::std::os::raw::c_char,
    ) -> *mut ::std::os::raw::c_char;
}
extern "C" {
    /// babl_format:
    ///
    /// Returns the babl object representing the color format given by
    /// @name such as for example \"RGB u8\", \"CMYK float\" or \"CIE Lab u16\",
    /// creates a format using the sRGB space, to also specify the color space
    /// and TRCs for a format, see babl_format_with_space.
    pub fn babl_format(encoding: *const ::std::os::raw::c_char) -> *const Babl;
}
extern "C" {
    /// babl_format_with_space:
    ///
    /// Returns the babl object representing the color format given by
    /// @name such as for example \"RGB u8\", \"R'G'B'A float\", \"Y float\" with
    /// a specific RGB working space used as the space, the resulting format
    /// has -space suffixed to it, unless the space requested is sRGB then
    /// the unsuffixed version is used. If a format is passed in as space
    /// the space of the format is used.
    pub fn babl_format_with_space(
        encoding: *const ::std::os::raw::c_char,
        space: *const Babl,
    ) -> *const Babl;
}
extern "C" {
    /// babl_format_exists:
    ///
    /// Returns 1 if the provided format name is known by babl or 0 if it is
    /// not. Can also be used to verify that specific extension formats are
    /// available (though this can also be inferred from the version of babl).
    pub fn babl_format_exists(name: *const ::std::os::raw::c_char) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn babl_format_get_space(format: *const Babl) -> *const Babl;
}
extern "C" {
    /// babl_fish:
    ///
    ///  Create a babl fish capable of converting from source_format to
    ///  destination_format, source and destination can be either strings
    ///  with the names of the formats or Babl-format objects.
    pub fn babl_fish(
        source_format: *const ::std::os::raw::c_void,
        destination_format: *const ::std::os::raw::c_void,
    ) -> *const Babl;
}
extern "C" {
    /// babl_fast_fish:
    ///
    /// Create a faster than normal fish with specified performance (and thus
    /// corresponding precision tradeoff), values tolerance can hold: NULL and
    /// \"default\", means do same as babl_fish(), other values understood in
    /// increasing order of speed gain are:
    ///    \"exact\" \"precise\" \"fast\" \"glitch\"
    ///
    /// Fast fishes should be cached, since they are not internally kept track
    /// of/made into singletons by babl and many creations of fast fishes will
    /// otherwise be a leak.
    ///
    pub fn babl_fast_fish(
        source_format: *const ::std::os::raw::c_void,
        destination_format: *const ::std::os::raw::c_void,
        performance: *const ::std::os::raw::c_char,
    ) -> *const Babl;
}
extern "C" {
    /// babl_process:
    ///
    ///  Process n pixels from source to destination using babl_fish,
    ///  returns number of pixels converted.
    pub fn babl_process(
        babl_fish: *const Babl,
        source: *const ::std::os::raw::c_void,
        destination: *mut ::std::os::raw::c_void,
        n: ::std::os::raw::c_long,
    ) -> ::std::os::raw::c_long;
}
extern "C" {
    pub fn babl_process_rows(
        babl_fish: *const Babl,
        source: *const ::std::os::raw::c_void,
        source_stride: ::std::os::raw::c_int,
        dest: *mut ::std::os::raw::c_void,
        dest_stride: ::std::os::raw::c_int,
        n: ::std::os::raw::c_long,
        rows: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_long;
}
extern "C" {
    /// babl_get_name:
    ///
    /// Returns a string describing a Babl object.
    pub fn babl_get_name(babl: *const Babl) -> *const ::std::os::raw::c_char;
}
extern "C" {
    /// babl_format_has_alpha:
    ///
    /// Returns whether the @format has an alpha channel.
    pub fn babl_format_has_alpha(format: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_format_get_bytes_per_pixel:
    ///
    /// Returns the bytes per pixel for a babl color format.
    pub fn babl_format_get_bytes_per_pixel(format: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_format_get_model:
    ///
    /// Return the model used for constructing the format.
    pub fn babl_format_get_model(format: *const Babl) -> *const Babl;
}
pub const BablModelFlag_BABL_MODEL_FLAG_ALPHA: BablModelFlag = 2;
pub const BablModelFlag_BABL_MODEL_FLAG_ASSOCIATED: BablModelFlag = 4;
pub const BablModelFlag_BABL_MODEL_FLAG_INVERTED: BablModelFlag = 8;
pub const BablModelFlag_BABL_MODEL_FLAG_LINEAR: BablModelFlag = 1024;
pub const BablModelFlag_BABL_MODEL_FLAG_NONLINEAR: BablModelFlag = 2048;
pub const BablModelFlag_BABL_MODEL_FLAG_PERCEPTUAL: BablModelFlag = 4096;
pub const BablModelFlag_BABL_MODEL_FLAG_GRAY: BablModelFlag = 1048576;
pub const BablModelFlag_BABL_MODEL_FLAG_RGB: BablModelFlag = 2097152;
pub const BablModelFlag_BABL_MODEL_FLAG_CIE: BablModelFlag = 8388608;
pub const BablModelFlag_BABL_MODEL_FLAG_CMYK: BablModelFlag = 16777216;

pub type BablModelFlag = ::std::os::raw::c_uint;
extern "C" {
    pub fn babl_get_model_flags(model: *const Babl) -> BablModelFlag;
}
extern "C" {
    /// babl_format_get_n_components:
    ///
    /// Returns the number of components for the given @format.
    pub fn babl_format_get_n_components(format: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_format_get_type:
    ///
    /// Returns the type in the given @format for the given
    /// @component_index.
    pub fn babl_format_get_type(
        format: *const Babl,
        component_index: ::std::os::raw::c_int,
    ) -> *const Babl;
}
extern "C" {
    /// babl_type_new:
    ///
    /// Defines a new data type in babl. A data type that babl can have in
    /// its buffers requires conversions to and from \"double\" to be
    /// registered before passing sanity.
    ///
    /// ```c
    ///     babl_type_new       (const char *name,
    ///                          \"bits\",     int bits,
    ///                          [\"min_val\", double min_val,]
    ///                          [\"max_val\", double max_val,]
    ///                          NULL);
    /// ```
    pub fn babl_type_new(first_arg: *mut ::std::os::raw::c_void, ...) -> *const Babl;
}
extern "C" {
    /// babl_component_new:
    ///
    /// Defines a new color component with babl.
    ///
    ///     babl_component_new  (const char *name,
    ///                          NULL);
    pub fn babl_component_new(first_arg: *mut ::std::os::raw::c_void, ...) -> *const Babl;
}
extern "C" {
    /// babl_model_new:
    ///
    /// Defines a new color model in babl. If no name is provided a name is
    /// generated by concatenating the name of all the involved components.
    ///
    /// ```c
    ///     babl_model_new      ([\"name\", const char *name,]
    ///                          BablComponent *component1,
    ///                          [BablComponent *componentN, ...]
    ///                          NULL);
    /// ```
    pub fn babl_model_new(first_arg: *mut ::std::os::raw::c_void, ...) -> *const Babl;
}
extern "C" {
    /// babl_format_new:
    ///
    /// Defines a new pixel format in babl. Provided BablType and|or
    /// BablSampling is valid for the following components as well. If no
    /// name is provided a (long) descriptive name is used.
    ///
    /// ```c
    ///     babl_format_new     ([\"name\", const char *name,]
    ///                          BablModel          *model,
    ///                          [BablType           *type,]
    ///                          [BablSampling,      *sampling,]
    ///                          BablComponent      *component1,
    ///                          [[BablType           *type,]
    ///                           [BablSampling       *sampling,]
    ///                           BablComponent      *componentN,
    ///                           ...]
    ///                          [\"planar\",]
    ///                          NULL);
    /// ```
    pub fn babl_format_new(first_arg: *const ::std::os::raw::c_void, ...) -> *const Babl;
}
extern "C" {
    pub fn babl_format_n(type_: *const Babl, components: ::std::os::raw::c_int) -> *const Babl;
}
extern "C" {
    /// babl_format_is_format_n:
    ///
    /// Returns whether the @format is a format_n type.
    pub fn babl_format_is_format_n(format: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_conversion_new:
    ///
    /// Defines a new conversion between either two formats, two models or
    /// two types in babl.
    ///
    /// ```c
    ///     babl_conversion_new (<BablFormat *source, BablFormat *destination|
    ///                          BablModel  *source, BablModel  *destination|
    ///                          BablType   *source, BablType   *destination>,
    ///                          <\"linear\"|\"planar\">, <BablFuncLinear | BablFuncPlanar> conv_func,
    ///                          NULL);
    /// ```
    pub fn babl_conversion_new(first_arg: *const ::std::os::raw::c_void, ...) -> *const Babl;
}
extern "C" {
    /// babl_conversion_get_source_space:
    ///
    /// Returns the RGB space defined for the source of conversion.
    pub fn babl_conversion_get_source_space(conversion: *const Babl) -> *const Babl;
}
extern "C" {
    /// babl_conversion_get_destination_space:
    ///
    /// Returns the RGB space defined for the destination of conversion.
    pub fn babl_conversion_get_destination_space(conversion: *const Babl) -> *const Babl;
}
extern "C" {
    /// babl_new_palette:
    ///
    /// create a new palette based format, name is optional pass in NULL to get
    /// an anonymous format. If you pass in with_alpha the format also gets
    /// an 8bit alpha channel. Returns the BablModel of the color model. If
    /// you pass in the same name the previous formats will be provided
    /// again.
    pub fn babl_new_palette(
        name: *const ::std::os::raw::c_char,
        format_u8: *mut *const Babl,
        format_u8_with_alpha: *mut *const Babl,
    ) -> *const Babl;
}
extern "C" {
    /// babl_new_palette_with_space:
    ///
    /// create a new palette based format, name is optional pass in NULL to get
    /// an anonymous format. If you pass in with_alpha the format also gets
    /// an 8bit alpha channel. Returns the BablModel of the color model. If
    /// you pass in the same name the previous formats will be provided
    /// again.
    pub fn babl_new_palette_with_space(
        name: *const ::std::os::raw::c_char,
        space: *const Babl,
        format_u8: *mut *const Babl,
        format_u8_with_alpha: *mut *const Babl,
    ) -> *const Babl;
}
extern "C" {
    /// babl_format_is_palette:
    ///
    /// check whether a format is a palette backed format.
    pub fn babl_format_is_palette(format: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_palette_set_palette:
    /// @babl: a #Babl
    /// @format: The pixel format
    /// @data: (array) (element-type guint8): The pixel data
    /// @count: The number of pixels in @data
    ///
    /// Assign a palette to a palette format, the data is a single span of pixels
    /// representing the colors of the palette.
    pub fn babl_palette_set_palette(
        babl: *const Babl,
        format: *const Babl,
        data: *mut ::std::os::raw::c_void,
        count: ::std::os::raw::c_int,
    );
}
extern "C" {
    /// babl_palette_reset:
    ///
    /// reset a palette to initial state, frees up some caches that optimize
    /// conversions.
    pub fn babl_palette_reset(babl: *const Babl);
}
extern "C" {
    /// babl_set_user_data: (skip)
    ///
    /// associate a data pointer with a format/model, this data can be accessed and
    /// used from the conversion functions, encoding color profiles, palettes or
    /// similar with the data, perhaps this should be made internal API, not
    /// accesible at all from
    pub fn babl_set_user_data(babl: *const Babl, data: *mut ::std::os::raw::c_void);
}
extern "C" {
    /// babl_get_user_data: (skip)
    ///
    /// Get data set with babl_set_user_data
    pub fn babl_get_user_data(babl: *const Babl) -> *mut ::std::os::raw::c_void;
}
pub const BablSpaceFlags_BABL_SPACE_FLAG_NONE: BablSpaceFlags = 0;
pub const BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE: BablSpaceFlags = 1;
pub type BablSpaceFlags = ::std::os::raw::c_uint;
extern "C" {
    /// babl_space_from_chromaticities:
    /// @name: (nullable): The name for the color space
    /// @wx: The X-coordinate of the color space's white point
    /// @wy: The Y-coordinate of the color space's white point
    /// @rx: The X-coordinate of the red primary
    /// @ry: The Y-coordinate of the red primary
    /// @gx: The X-coordinate of the green primary
    /// @gy: The Y-coordinate of the green primary
    /// @bx: The X-coordinate of the blue primary
    /// @by: The Y-coordinate of the blue primary
    /// @trc_red: The red component of the TRC.
    /// @trc_green: (nullable): The green component of the TRC (can be %NULL if it's
    ///            the same as @trc_red).
    /// @trc_blue: (nullable): The blue component of the TRC (can be %NULL if it's
    ///            the same as @trc_red).
    /// @flags: The #BablSpaceFlags
    ///
    /// Creates a new babl-space/ RGB matrix color space definition with the
    /// specified CIE xy(Y) values for white point: wx, wy and primary
    /// chromaticities: rx,ry,gx,gy,bx,by and TRCs to be used. After registering a
    /// new babl-space it can be used with babl_space() passing its name;
    ///
    /// Internally this does the math to derive the RGBXYZ matrix as used in an ICC
    /// profile.
    pub fn babl_space_from_chromaticities(
        name: *const ::std::os::raw::c_char,
        wx: f64,
        wy: f64,
        rx: f64,
        ry: f64,
        gx: f64,
        gy: f64,
        bx: f64,
        by: f64,
        trc_red: *const Babl,
        trc_green: *const Babl,
        trc_blue: *const Babl,
        flags: BablSpaceFlags,
    ) -> *const Babl;
}
extern "C" {
    /// babl_trc_gamma:
    ///
    /// Creates a Babl TRC for a specific gamma value, it will be given
    /// a name that is a short string representation of the value.
    pub fn babl_trc_gamma(gamma: f64) -> *const Babl;
}
extern "C" {
    /// babl_trc:
    ///
    /// Look up a TRC by name, \"sRGB\" and \"linear\" are recognized
    /// strings in a stock babl configuration.
    pub fn babl_trc(name: *const ::std::os::raw::c_char) -> *const Babl;
}
extern "C" {
    /// babl_space_with_trc:
    ///
    /// Creates a variant of an existing space with different trc.
    pub fn babl_space_with_trc(space: *const Babl, trc: *const Babl) -> *const Babl;
}
extern "C" {
    /// babl_space_get:
    /// @space: A #Babl instance
    /// @xw: (out) (optional): The X-coordinate of the color space's white point
    /// @yw: (out) (optional): The Y-coordinate of the color space's white point
    /// @xr: (out) (optional): The X-coordinate of the red primary
    /// @yr: (out) (optional): The Y-coordinate of the red primary
    /// @xg: (out) (optional): The X-coordinate of the blue primary
    /// @yg: (out) (optional): The Y-coordinate of the green primary
    /// @xb: (out) (optional): The X-coordinate of the blue primary
    /// @yb: (out) (optional): The Y-coordinate of the blue primary
    /// @red_trc: (out) (optional): The red component of the TRC.
    /// @green_trc: (out) (optional): The green component of the TRC (can be %NULL
    ///             if it's the same as @red_trc).
    /// @blue_trc: (out) (optional): The blue component of the TRC (can be %NULL if
    ///            it's the same as @red_trc).
    ///
    /// query the chromaticities of white point and primaries as well as trcs
    /// used for r g a nd b, all arguments are optional (can be %NULL).
    pub fn babl_space_get(
        space: *const Babl,
        xw: *mut f64,
        yw: *mut f64,
        xr: *mut f64,
        yr: *mut f64,
        xg: *mut f64,
        yg: *mut f64,
        xb: *mut f64,
        yb: *mut f64,
        red_trc: *mut *const Babl,
        green_trc: *mut *const Babl,
        blue_trc: *mut *const Babl,
    );
}
extern "C" {
    /// babl_space_get_rgb_luminance:
    /// @space: a BablSpace
    /// @red_luminance: (out) (optional): Location for the red luminance factor.
    /// @green_luminance: (out) (optional): Location for the green luminance factor.
    /// @blue_luminance: (out) (optional): Location for the blue luminance factor.
    ///
    /// Retrieve the relevant RGB luminance constants for a babl space.
    pub fn babl_space_get_rgb_luminance(
        space: *const Babl,
        red_luminance: *mut f64,
        green_luminance: *mut f64,
        blue_luminance: *mut f64,
    );
}
extern "C" {
    pub fn babl_space_get_gamma(space: *const Babl) -> f64;
}

extern "C" {
    /// babl_model_is:
    ///
    /// Returns: 0 if the name of the model in babl does not correspond to the
    /// provided model name.
    pub fn babl_model_is(
        babl: *const Babl,
        model_name: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    /// babl_space_get_icc:
    /// @babl: a #Babl
    /// @length: (out) (optional): Length of the profile in bytes.
    ///
    /// Return pointer to ICC profile for space note that this is
    /// the ICC profile for R'G'B', though in formats only supporting linear
    /// like EXR GEGL chooses to load this lienar data as RGB and use the sRGB
    /// TRC.
    ///
    /// Returns: pointer to ICC profile data.
    pub fn babl_space_get_icc(
        babl: *const Babl,
        length: *mut ::std::os::raw::c_int,
    ) -> *const ::std::os::raw::c_char;
}
extern "C" {
    /// babl_space_from_rgbxyz_matrix:
    /// @name: (nullable): The name for the color space
    /// @wx: The X-coordinate of the color space's white point
    /// @wy: The Y-coordinate of the color space's white point
    /// @wz: The Z-coordinate of the color space's white point
    /// @rx: The X-coordinate of the red primary
    /// @ry: The Y-coordinate of the red primary
    /// @rz: The Z-coordinate of the red primary
    /// @gx: The X-coordinate of the green primary
    /// @gy: The Y-coordinate of the green primary
    /// @gz: The Z-coordinate of the green primary
    /// @bx: The X-coordinate of the blue primary
    /// @by: The Y-coordinate of the blue primary
    /// @bz: The Z-coordinate of the blue primary
    /// @trc_red: The red component of the TRC.
    /// @trc_green: (nullable): The green component of the TRC (can be %NULL if it's
    ///            the same as @trc_red).
    /// @trc_blue: (nullable): The blue component of the TRC (can be %NULL if it's
    ///            the same as @trc_red).
    ///
    /// Creates a new RGB matrix color space definition using a precomputed D50
    /// adapted 3x3 matrix and associated CIE XYZ whitepoint, as possibly read from
    /// an ICC profile.
    pub fn babl_space_from_rgbxyz_matrix(
        name: *const ::std::os::raw::c_char,
        wx: f64,
        wy: f64,
        wz: f64,
        rx: f64,
        gx: f64,
        bx: f64,
        ry: f64,
        gy: f64,
        by: f64,
        rz: f64,
        gz: f64,
        bz: f64,
        trc_red: *const Babl,
        trc_green: *const Babl,
        trc_blue: *const Babl,
    ) -> *const Babl;
}
extern "C" {
    /// babl_format_get_encoding:
    ///
    /// Returns the components and data type, without space suffix.
    pub fn babl_format_get_encoding(babl: *const Babl) -> *const ::std::os::raw::c_char;
}
extern "C" {
    pub fn babl_space_is_cmyk(space: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn babl_space_is_gray(space: *const Babl) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn babl_introspect(babl: *const Babl);
}
